package com.iws.isobar.awarenessapi

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

import com.google.android.gms.awareness.Awareness
import com.google.android.gms.awareness.fence.AwarenessFence
import com.google.android.gms.awareness.fence.DetectedActivityFence
import com.google.android.gms.awareness.fence.FenceState
import com.google.android.gms.awareness.fence.FenceUpdateRequest
import com.google.android.gms.awareness.fence.HeadphoneFence
import com.google.android.gms.awareness.state.HeadphoneState
import com.google.android.gms.common.api.GoogleApiClient

class MainActivity : AppCompatActivity() {
    private var pendingIntent: PendingIntent? = null
    private var receiver: FenceReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = Intent(FENCE_RECEIVER_ACTION)

        //Pending intent that will be used to get callbacks from fence
        pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)
        receiver =  FenceReceiver()
        registerReceiver(receiver, IntentFilter(FENCE_RECEIVER_ACTION))

        //Creating the Awareness Client
        val client = GoogleApiClient.Builder(this)
                .addApi(Awareness.API)
                .build()
        client.connect()

    }

    override fun onStop() {
        super.onStop()
        try {
            unregisterReceiver(receiver)
            //unregister receiver in case it makes sense
            //unregisterReceiver(myFenceReceiver);
        } catch (e: IllegalArgumentException) {
        }

    }

    companion object {
        private val FENCE_RECEIVER_ACTION = "com.iws.isoba.awarenessapi"
    }

}
