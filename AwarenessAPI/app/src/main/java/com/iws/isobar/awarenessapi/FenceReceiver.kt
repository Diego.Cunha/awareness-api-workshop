package com.iws.isobar.awarenessapi

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.google.android.gms.awareness.fence.FenceState

/**
 * Created by diegocunha on 28/11/17.
 */
class FenceReceiver : BroadcastReceiver() {

    override fun onReceive(p0: Context?, p1: Intent?) {
        val fenceState: FenceState = FenceState.extract(p1)


        if (fenceState.fenceKey.equals("someFence")) {
            when (fenceState.currentState) {
                FenceState.TRUE -> print("SOMETHING JUST HAPPENED!")
                else -> print("Deu ruim!")
            }
        }
    }
}